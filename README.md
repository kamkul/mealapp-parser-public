Wersa Maven
```
3.6.3
```

Uwaga - jeśli nie masz działającego Gradle to potrzebujesz ściągnąć:
```
http://ftp.man.poznan.pl/apache/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.zip
```
Wypakować w na przykład 
```
C:\Maven\
```

A następnie dodać zmienną środowiskową
```
C:\Maven\apache-maven-3.6.3\bin
```

Następnie zweryfikować czy Maven działa poprzez wykonanie w cmd
```
mvn -v
```
Jeśli komenda się wykona to gut, pamiętaj aby uruchomić cmd na nowo po dodaniu zmiennej środowiskowej!

Kompilacja
```
mvn install
mvn clean package
```

Skompilowany projekt znajduje się w katalogu target


Co potrzebujesz, aby odpalić ten projekt?
```
https://www.docker.com/products/docker-desktop
```

Jeśli korzystasz z Windows 10, naciśnij "Download for Windows", następnie zainstaluj.

Uwaga, jeśli korzystasz z VirtualBoxa, przestanie on działać po instalacji Dockera, aby przywrócić działanie VirtualBoxa począwszy od wersji 6, należy wkleić taką linię do cmd uruchomionego jako admin
```
VBoxManage setextradata global "VBoxInternal/NEM/UseRing0Runloop" 0
```

Klonujemy projekt, aby go uruchomić wpisujemy w cmd, pracującym na katalogu głównym projektu
```
docker-compose up --build
```

Przygotował: Kamil