package domain.common;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class BaseEatingHouseHandler {
    private static final DateTimeFormatter df = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    protected BaseEatingHouseHandler() {
    }

    public static LocalDate parseStringToLocalDate(String date) {
        return LocalDate.parse(date, df);
    }

    public static int dateDiff(Date date1, Date date2) {
        long x = date1.getTime() - date2.getTime();
        x /= (1000 * 60 * 60 * 24);
        return (int) x;
    }

    public static String parseLocalDateTimeToString(LocalDate date) {
        return date.format(df);
    }

    public static LocalDate getCurrentDayLocalDate() {
        return LocalDate.now();
    }
}
