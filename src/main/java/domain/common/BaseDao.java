package domain.common;

import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

public class BaseDao<T, Id extends Serializable> {
    private EntityManager currentEntityManager;
    private EntityTransaction currentTransaction;
    private Class<T> tClass;

    public BaseDao(Class<T> tClass) {
        this.tClass = tClass;
    }

    public EntityManager openCurrentSession() {
        currentEntityManager = getEntityManagerFactory().createEntityManager();
        return currentEntityManager;
    }

    public EntityManager openCurrentSessionwithTransaction() {
        currentEntityManager = getEntityManagerFactory().createEntityManager();
        currentTransaction = currentEntityManager.getTransaction();
        currentTransaction.begin();
        return currentEntityManager;
    }

    public void closeCurrentSession() {
        currentEntityManager.close();
    }

    public void closeCurrentSessionwithTransaction() {
        currentTransaction.commit();
        currentEntityManager.close();
    }

    private static EntityManagerFactory getEntityManagerFactory() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("MealApiDB");
        return factory;
    }

    public EntityManager getCurrentEntityManager() {
        return currentEntityManager;
    }

    public void setCurrentEntityManager(Session currentEntityManager) {
        this.currentEntityManager = currentEntityManager;
    }

    public EntityTransaction getCurrentTransaction() {
        return currentTransaction;
    }

    public void setCurrentTransaction(Transaction currentTransaction) {
        this.currentTransaction = currentTransaction;
    }

    public void persist(T entity) {
        getCurrentEntityManager().persist(entity);
    }

    public void update(T entity) {
        getCurrentEntityManager().merge(entity);
    }

    public T findById(Id id) {
        T object = (T) getCurrentEntityManager().find(tClass, id);
        return object;
    }

    public List<T> findAll() {
        return (List<T>) getCurrentEntityManager().createQuery("from " + tClass.getCanonicalName()).getResultList();
    }

    public void delete(Id id) {
        String deleteQuery = "DELETE FROM Content WHERE id= :id";
        Query query = getCurrentEntityManager().createQuery(deleteQuery);
        query.setParameter("id", id);
        query.executeUpdate();
    }

}