package domain.menu;

import domain.common.BaseDao;
import domain.eatingHouse.EatingHouse;

import javax.persistence.TypedQuery;
import java.util.List;

public class MenuDao extends BaseDao<Menu, Integer> {

    public MenuDao() {
        super(Menu.class);
    }

    public List<Menu> loadMenuList(EatingHouse eatingHouse) {
        TypedQuery<Menu> query = getCurrentEntityManager()
                .createQuery("from Menu menu where menu.eatingHouse.id=:eatingHouseId", Menu.class);
        query.setParameter("eatingHouseId", eatingHouse.getId());
        query.setMaxResults(30);
        
        return query.getResultList();
    }
}


