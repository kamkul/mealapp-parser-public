package domain.menu;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public class MenuHelper {

    private MenuHelper() {
    }

    public static boolean containsDate(List<Menu> menuList, LocalDate date) {
        for (Menu menu : menuList) {
            if (menu.getDate().equals(date))
                return true;
        }
        return false;
    }

    public static Menu getMenuByDate(List<Menu> menuList, LocalDate date) {
        for (Menu menu : menuList) {
            if (menu.getDate().equals(date))
                return menu;
        }
        throw new NullPointerException();
    }
}
