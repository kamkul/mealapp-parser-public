package domain.menu;

import domain.eatingHouse.EatingHouse;

import java.util.List;

public class MenuService {
    private static MenuDao dao;

    public MenuService() {
        dao = new MenuDao();
    }

    public void persist(Menu entity) {
        dao.openCurrentSessionwithTransaction();
        dao.persist(entity);
        dao.closeCurrentSessionwithTransaction();
    }

    public void update(Menu entity) {
        dao.openCurrentSessionwithTransaction();
        dao.update(entity);
        dao.closeCurrentSessionwithTransaction();
    }

    public Menu findById(Integer id) {
        dao.openCurrentSession();
        Menu menu = dao.findById(id);
        dao.closeCurrentSession();
        return menu;
    }

    public List<Menu> findAll() {
        dao.openCurrentSession();
        List<Menu> MenuList = dao.findAll();
        dao.closeCurrentSession();
        return MenuList;
    }

    public List<Menu> loadMenuList(EatingHouse eatingHouse) {
        dao.openCurrentSession();
        List<Menu> menuList = dao.loadMenuList(eatingHouse);
        dao.closeCurrentSession();
        return menuList;
    }
    
}
