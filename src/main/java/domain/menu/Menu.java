package domain.menu;

import domain.content.Content;
import domain.eatingHouse.EatingHouse;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Entity
public class Menu {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private LocalDate date;


    @OneToMany(cascade = {CascadeType.REMOVE, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, fetch = FetchType.EAGER, mappedBy = "menu", orphanRemoval = true)
    private List<Content> contentList;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idEatingHouse")
    private EatingHouse eatingHouse;

    public List<Content> getContentList() {
        return contentList;
    }

    public void setContentList(List<Content> contentList) {

        for (Content content : contentList) {
            content.setMenu(this);
        }

        this.contentList = contentList;
    }

    public EatingHouse getEatingHouse() {
        return eatingHouse;
    }

    public void setEatingHouse(EatingHouse eatingHouse) {
        this.eatingHouse = eatingHouse;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Menu{" +
                "id=" + id +
                ", date=" + date +
                '}';
    }

}
