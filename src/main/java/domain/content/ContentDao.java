package domain.content;

import domain.common.BaseDao;

public class ContentDao extends BaseDao<Content, Integer> {

    public ContentDao() {
        super(Content.class);
    }
}


