package domain.content;

import java.util.List;

public class ContentService {

    private static ContentDao dao;

    public ContentService() {
        dao = new ContentDao();
    }

    public void persist(Content entity) {
        dao.openCurrentSessionwithTransaction();
        dao.persist(entity);
        dao.closeCurrentSessionwithTransaction();
    }

    public void update(Content entity) {
        dao.openCurrentSessionwithTransaction();
        dao.update(entity);
        dao.closeCurrentSessionwithTransaction();
    }

    public Content findById(Integer id) {
        dao.openCurrentSession();
        Content content = dao.findById(id);
        dao.closeCurrentSession();
        return content;
    }

    public List<Content> findAll() {
        dao.openCurrentSession();
        List<Content> ContentList = dao.findAll();
        dao.closeCurrentSession();
        return ContentList;
    }

    public void delete(Integer id) {
        dao.openCurrentSessionwithTransaction();
        dao.delete(id);
        dao.closeCurrentSessionwithTransaction();
    }

}
