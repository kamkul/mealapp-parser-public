package domain.eatingHouse;

import domain.common.BaseDao;
import domain.menu.Menu;

import javax.persistence.TypedQuery;
import java.util.List;

public class EatingHouseDao extends BaseDao<EatingHouse, Integer> {

    public EatingHouseDao() {
        super(EatingHouse.class);
    }

    public EatingHouse findByName(String name) {
        TypedQuery<EatingHouse> query = getCurrentEntityManager()
                .createQuery("from EatingHouse eh where eh.name=:name", EatingHouse.class);
        query.setParameter("name", name);
        return query.getSingleResult();
    }

}


