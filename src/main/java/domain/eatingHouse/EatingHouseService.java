package domain.eatingHouse;

import domain.menu.Menu;

import java.util.List;

public class EatingHouseService {

    private static EatingHouseDao dao;

    public EatingHouseService() {
        dao = new EatingHouseDao();
    }

    public void persist(EatingHouse entity) {
        dao.openCurrentSessionwithTransaction();
        dao.persist(entity);
        dao.closeCurrentSessionwithTransaction();
    }

    public void update(EatingHouse entity) {
        dao.openCurrentSessionwithTransaction();
        dao.update(entity);
        dao.closeCurrentSessionwithTransaction();
    }

    public EatingHouse findById(Integer id) {
        dao.openCurrentSession();
        EatingHouse eatingHouse = dao.findById(id);
        dao.closeCurrentSession();
        return eatingHouse;
    }

    public EatingHouse findByName(String name) {
        dao.openCurrentSession();
        EatingHouse eatingHouse = dao.findByName(name);
        dao.closeCurrentSession();
        return eatingHouse;
    }

    public List<EatingHouse> findAll() {
        dao.openCurrentSession();
        List<EatingHouse> eatingHouseList = dao.findAll();
        dao.closeCurrentSession();
        return eatingHouseList;
    }

}
