package parser.pelnyGar;

import domain.content.Content;
import domain.content.ContentService;
import domain.eatingHouse.EatingHouse;
import domain.eatingHouse.EatingHouseService;
import domain.menu.Menu;
import domain.menu.MenuHelper;
import domain.menu.MenuService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.time.LocalDate;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PelnyGarParser {

    public static final String BASE_URL = "http://www.pelnygar.pl/";
    public static final String CONTENT_URL = "menu-tygodnia";
    public static final String NAME = "Pełny Gar";
    private static final String STATIC_CONTENT_CSS_QUERY = "div.main > div.container-fluid > div[style=text-align: center;] b, div table.tabelaMenTyg tr";
    private static final String DAILY_CONTENT_CSS_QUERY = "[data-label~=(\\d{2}-\\d{2}-\\d{4})]";

    private final ContentService contentService;
    private final EatingHouseService eatingHouseService;
    private final MenuService menuService;
    private final EatingHouse eatingHouse;
    private final Document document;

    public PelnyGarParser() throws IOException {
        contentService = new ContentService();
        eatingHouseService = new EatingHouseService();
        menuService = new MenuService();
        eatingHouse = eatingHouseService.findByName(NAME);
        document = Jsoup.connect(BASE_URL + CONTENT_URL).get();
    }

    private Content parseStaticContent() {
        StringBuilder stringBuilder = new StringBuilder();

        System.out.println("STATIC CONTENT CSS QUERY TEST");

        Elements paragraphs = document.select(STATIC_CONTENT_CSS_QUERY);

        for (Element paragraph : paragraphs) {
            if (paragraph.hasText())
                stringBuilder.append(paragraph.text()).append("\n");
        }

        Content content = new Content();
        content.setType("STATIC");
        content.setContent(stringBuilder.deleteCharAt(stringBuilder.length() - 1).toString());

        return content;
    }

    private HashMap<LocalDate, List<Content>> parseDailyContent() {
        HashMap<LocalDate, List<Content>> output = new HashMap<>();

        Elements paragraphs = document.select(DAILY_CONTENT_CSS_QUERY);
        Pattern pattern = Pattern.compile("(\\d{2}-\\d{2}-\\d{4})");

        List<Content> contentList;
        Content content;

        for (Element paragraph : paragraphs) {
            String date;
            Matcher matcher = pattern.matcher(paragraph.text());

            if (matcher.find()) {
                contentList = new ArrayList<Content>();
                date = matcher.group(1);

                String[] splittedMenu = PelnyGarHandler.menuSplit(paragraph.text());

                for (int i = 1; i < splittedMenu.length; i += 2) {
                    content = new Content();
                    content.setType("DAILY");

                    String menu = splittedMenu[i] + "\n" + splittedMenu[i + 1];
                    content.setContent(menu);

                    contentList.add(content);
                }

                output.put(PelnyGarHandler.parseStringToLocalDate(date), contentList);
            }
        }

        System.out.println(output.toString());

        return output;
    }

    public void parseEatingHouse() {
        HashMap<LocalDate, List<Content>> dailyContent = parseDailyContent();
        Content staticContent = parseStaticContent();

        List<Menu> loadedMenuList = menuService.loadMenuList(eatingHouse);

        List<Menu> menuList = new ArrayList<>();

        for (Map.Entry<LocalDate, List<Content>> entry : dailyContent.entrySet()) {
            Menu menu;
            entry.getValue().add(staticContent);

            if (MenuHelper.containsDate(loadedMenuList, entry.getKey())) {
                menu = MenuHelper.getMenuByDate(loadedMenuList, entry.getKey());
                List<Content> contentList = menu.getContentList();

                int i = 0;
                for (; i < entry.getValue().size(); i++) {
                    if (i < contentList.size()) {
                        contentList.get(i).setContent(entry.getValue().get(i).getContent());
                        contentList.get(i).setType(entry.getValue().get(i).getType());
                        contentService.update(contentList.get(i));
                    } else {
                        Content newContent = new Content();
                        newContent.setContent(entry.getValue().get(i).getContent());
                        newContent.setType(entry.getValue().get(i).getType());
                        newContent.setMenu(menu);
                        contentService.persist(newContent);
                    }
                }

                for (int j = i; j < contentList.size(); j++) {
                    contentService.delete(contentList.get(j).getId());
                }
            } else {
                menu = new Menu();
                menu.setContentList(entry.getValue());
                menu.setDate(entry.getKey());
                menuList.add(menu);
            }
        }

        eatingHouse.setMenuList(menuList);
        eatingHouseService.update(eatingHouse);
    }

}
