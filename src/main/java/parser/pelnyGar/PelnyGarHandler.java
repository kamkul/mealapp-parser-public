package parser.pelnyGar;

import domain.common.BaseEatingHouseHandler;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PelnyGarHandler extends BaseEatingHouseHandler {

    private static final DateFormat df = new SimpleDateFormat("dd-MM-yyyy");

    private PelnyGarHandler() {
    }

    public static String[] menuSplit(String string) {
        return string.split("(?=Zupa:)|(?=Danie główne:)");
    }

    public static String deleteDates(String string) {
        String regex = "Poniedziałek\\s|Wtorek\\s|Środa\\s|Czwartek\\s|Piątek\\s|Sobota\\s|Niedziela\\s|\\d{2}-\\d{2}-\\d{4}\\s*";
        return string.replaceAll(regex, "");
    }



}
