package parser;

import parser.bistroKociolek.BistroKociolekParser;
import parser.feta.FetaParser;
import parser.stolowkaWarminska.StolowkaWarminskaParser;

import java.io.IOException;

public class Application {

    public static void main(String[] args) {
        try {
//            Strona PełnegoGara wywala 404
//            PelnyGarParser pelnyGarParser = new PelnyGarParser();
//            pelnyGarParser.parseEatingHouse();

            StolowkaWarminskaParser stolowkaWarminskaParser = new StolowkaWarminskaParser();
            stolowkaWarminskaParser.parseEatingHouse();

//            Ciężko napisać parser
//            BistroKociolekParser bistroKociolekParser = new BistroKociolekParser();
//            bistroKociolekParser.parseEatingHouse();

            FetaParser fetaParser = new FetaParser();
            fetaParser.parseEatingHouse();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
