package parser.bistroKociolek;

import domain.content.Content;
import domain.content.ContentService;
import domain.eatingHouse.EatingHouse;
import domain.eatingHouse.EatingHouseService;
import domain.menu.MenuService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class BistroKociolekParser {
    public static final String BASE_URL = "https://www.bistro-kociolek.pl/";
    public static final String NAME = "Stołówka Warmińska";
    private static final String STATIC_CONTENT_CSS_QUERY = "div.content > div.menu";
    private static final String DAILY_CONTENT_CSS_QUERY = "div.eleven > ul.headerInfo";
    private static final String DAILY_CONTENT_DATE_CSS_QUERY = "body > b > font";

    private final ContentService contentService;
    private final EatingHouseService eatingHouseService;
    private final MenuService menuService;
    private final EatingHouse eatingHouse;
    private final Document document;

    public BistroKociolekParser() throws IOException {
        contentService = new ContentService();
        eatingHouseService = new EatingHouseService();
        menuService = new MenuService();
        eatingHouse = eatingHouseService.findByName(NAME);
        document = Jsoup.connect(BASE_URL).get();
    }

    private Content parseStaticContent() {
        StringBuilder stringBuilder = new StringBuilder();

        System.out.println("STATIC CONTENT CSS QUERY TEST");

        Elements paragraphs = document.select(STATIC_CONTENT_CSS_QUERY);

        for (Element paragraph : paragraphs) {
            if (paragraph.hasText())
                stringBuilder.append(paragraph.text()).append("\n");
        }

        Content content = new Content();
        content.setType("STATIC");
        content.setContent(stringBuilder.deleteCharAt(stringBuilder.length() - 1).toString());

        System.out.println(stringBuilder.toString());

        return content;
    }

    private HashMap<Date, List<Content>> parseDailyContent() {
        HashMap<Date, List<Content>> output = new HashMap<>();

        Elements paragraphsWithContent = document.select(DAILY_CONTENT_CSS_QUERY);

        for (Element paragraph : paragraphsWithContent) {
            System.out.println(paragraph.text());
        }

//        List<Content> contentList = new ArrayList<>();
//        Content content;
//        List<Date> dates = StolowkaWarminskaHandler.getDatesFromHeader(paragraphsWithDate.first().text());
//
//        for (Element paragraph : paragraphsWithContent) {
//            String[] splittedMenu = StolowkaWarminskaHandler.menuSplit(paragraph.html());
//
//            for (int i = 0; i < splittedMenu.length; i++) {
//                splittedMenu[i] = StolowkaWarminskaHandler.capitalizeString(Jsoup.parse(splittedMenu[i]).text());
//            }
//
//            content = new Content();
//            content.setType("DAILY");
//
//            content.setContent(
//                    String.join("\n", splittedMenu)
//            );
//
//            contentList.add(content);
//        }
//
//        for (int i = 0; i < dates.size(); i++) {
//            List<Content> contents = new ArrayList<>();
//            contents.add(contentList.get(i));
//            contents.add(contentList.get(i + dates.size()));
//            output.put(dates.get(i), contents);
//        }

//        output.forEach(
//                (key, value) -> {
//                    System.out.println(key.toString() + " DATA");
//                    value.forEach((v) -> {
//                        System.out.println(v.getContent() + "\n");
//                    });
//                });

        return output;
    }

    public void parseEatingHouse() {
//        HashMap<Date, List<Content>> dailyContent = parseDailyContent();
//        Content staticContent = parseStaticContent();
//
//        List<Menu> loadedMenuList = eatingHouse.getMenuList();
//
//        List<Menu> menuList = new ArrayList<>();
//
//        for (Map.Entry<Date, List<Content>> entry : dailyContent.entrySet()) {
//            Menu menu;
//            if (entry.getKey().equals(BaseEatingHouseHandler.getCurrentDayDate()))
//                entry.getValue().add(staticContent);
//
//            if (MenuHelper.containsDate(loadedMenuList, entry.getKey())) {
//                menu = MenuHelper.getMenuByDsate(loadedMenuList, entry.getKey());
//                menu.setContentList(entry.getValue());
//            } else {
//                menu = new Menu();
//                menu.setContentList(entry.getValue());
//                menu.setDate(entry.getKey());
//                menuList.add(menu);
//            }
//        }

//        eatingHouse.setMenuList(menuList);
//        eatingHouseService.update(eatingHouse);
        parseDailyContent();
//        parseStaticContent();
    }
}
