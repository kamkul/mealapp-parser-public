package parser.stolowkaWarminska;

import domain.common.BaseEatingHouseHandler;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StolowkaWarminskaHandler extends BaseEatingHouseHandler {

    private static final DateTimeFormatter df = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    private StolowkaWarminskaHandler() {
    }

    public static LocalDate parseStringToLocalDate(String date) {
        return LocalDate.parse(date, df);
    }

    public static String parseDateToString(LocalDate date) {
        return df.format(date);
    }

    public static List<LocalDate> getDatesFromHeader(String header) {
        final Pattern patternString = Pattern.compile("\\d{2}\\.\\d{2}\\.\\d{4}");
        List<LocalDate> output = new ArrayList<>();
        List<String> matchedDateStrings = new ArrayList<>();

        Matcher m = patternString.matcher(header);
        while (m.find()) {
            matchedDateStrings.add(m.group());
        }

        LocalDate start = parseStringToLocalDate(matchedDateStrings.get(0));
        LocalDate end = parseStringToLocalDate(matchedDateStrings.get(1));

        while (!start.equals(end.plusDays(1))) {
            output.add(start);
            start = start.plusDays(1);
        }

        return output;
    }

    public static String[] menuSplit(String html) {
        return html.split("<br><font color=\"maroon\">");
    }

    public static String capitalizeString(String input) {
        String firstLetter = input.substring(0, 1).toUpperCase();
        String rest = input.substring(1).toLowerCase();

        return firstLetter + rest;
    }
}
