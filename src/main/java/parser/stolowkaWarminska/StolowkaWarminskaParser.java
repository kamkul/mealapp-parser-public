package parser.stolowkaWarminska;

import domain.common.BaseEatingHouseHandler;
import domain.content.Content;
import domain.content.ContentService;
import domain.eatingHouse.EatingHouse;
import domain.eatingHouse.EatingHouseService;
import domain.menu.Menu;
import domain.menu.MenuHelper;
import domain.menu.MenuService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.time.LocalDate;
import java.util.*;

public class StolowkaWarminskaParser {

    public static final String BASE_URL = "http://www.stolowka-warminska.pl/";
    public static final String STATIC_CONTENT_URL = "menu.html";
    public static final String DAILY_CONTENT_URL = "tabelka.html";
    public static final String NAME = "Stołówka Warmińska";
    private static final String STATIC_CONTENT_CSS_QUERY = "div.content > div.menu";
    private static final String DAILY_CONTENT_CSS_QUERY = "table.table1 > tbody td";
    private static final String DAILY_CONTENT_DATE_CSS_QUERY = "body > b > font";

    private final ContentService contentService;
    private final EatingHouseService eatingHouseService;
    private final MenuService menuService;
    private final EatingHouse eatingHouse;
    private final Document dailyDocument;
    private final Document staticDocument;

    public StolowkaWarminskaParser() throws IOException {
        contentService = new ContentService();
        eatingHouseService = new EatingHouseService();
        menuService = new MenuService();
        eatingHouse = eatingHouseService.findByName(NAME);
        dailyDocument = Jsoup.connect(BASE_URL + DAILY_CONTENT_URL).get();
        staticDocument = Jsoup.connect(BASE_URL + STATIC_CONTENT_URL).get();
    }

    private Content parseStaticContent() {
        StringBuilder stringBuilder = new StringBuilder();

//        System.out.println("STATIC CONTENT CSS QUERY TEST");

        Elements paragraphs = staticDocument.select(STATIC_CONTENT_CSS_QUERY);

        for (Element paragraph : paragraphs) {
            if (paragraph.hasText())
                stringBuilder.append(paragraph.text()).append("\n");
        }

        Content content = new Content();
        content.setType("STATIC");
        content.setContent(stringBuilder.deleteCharAt(stringBuilder.length() - 1).toString());

//        System.out.println("BBBBBBBBBBBBBBBB\n\n\n\n\n\n\n\n\n" + stringBuilder.toString());

        return content;
    }

    private HashMap<LocalDate, List<Content>> parseDailyContent() {
        HashMap<LocalDate, List<Content>> output = new HashMap<>();

        Elements paragraphsWithContent = dailyDocument.select(DAILY_CONTENT_CSS_QUERY);
        Elements paragraphsWithDate = dailyDocument.select(DAILY_CONTENT_DATE_CSS_QUERY);

        List<Content> contentList = new ArrayList<>();
        Content content;
        List<LocalDate> dates = StolowkaWarminskaHandler.getDatesFromHeader(paragraphsWithDate.first().text());

        for (Element paragraph : paragraphsWithContent) {
            String[] splittedMenu = StolowkaWarminskaHandler.menuSplit(paragraph.html());

            for (int i = 0; i < splittedMenu.length; i++) {
                splittedMenu[i] = StolowkaWarminskaHandler.capitalizeString(Jsoup.parse(splittedMenu[i]).text());
            }

            content = new Content();
            content.setType("DAILY");

            content.setContent(
                    String.join("\n", splittedMenu)
            );

            contentList.add(content);
        }

        for (int i = 0; i < dates.size(); i++) {
            List<Content> contents = new ArrayList<>();
            contents.add(contentList.get(i));
            contents.add(contentList.get(i + dates.size()));
            output.put(dates.get(i), contents);
        }

//        output.forEach(
//                (key, value) -> {
//                    System.out.println(key.toString() + " DATA");
//                    value.forEach((v) -> {
//                        System.out.println(v.getContent() + "\n");
//                    });
//                });

        return output;
    }

    public void parseEatingHouse() {
        HashMap<LocalDate, List<Content>> dailyContent = parseDailyContent();
        Content staticContent = parseStaticContent();

        List<Menu> loadedMenuList = eatingHouse.getMenuList();

        List<Menu> menuList = new ArrayList<>();

        for (Map.Entry<LocalDate, List<Content>> entry : dailyContent.entrySet()) {
            Menu menu;
            if (entry.getKey().equals(BaseEatingHouseHandler.getCurrentDayLocalDate()))
                entry.getValue().add(staticContent);

            if (MenuHelper.containsDate(loadedMenuList, entry.getKey())) {
                menu = MenuHelper.getMenuByDate(loadedMenuList, entry.getKey());
                List<Content> contentList = menu.getContentList();

                int i = 0;
                for (; i < entry.getValue().size(); i++) {
                    if (i < contentList.size()) {
                        contentList.get(i).setContent(entry.getValue().get(i).getContent());
                        contentList.get(i).setType(entry.getValue().get(i).getType());
                        contentService.update(contentList.get(i));
                    } else {
                        Content newContent = new Content();
                        newContent.setContent(entry.getValue().get(i).getContent());
                        newContent.setType(entry.getValue().get(i).getType());
                        newContent.setMenu(menu);
                        contentService.persist(newContent);
                    }
                }

                for (int j = i; j < contentList.size(); j++) {
                    contentService.delete(contentList.get(j).getId());
                }

            } else {
                menu = new Menu();
                menu.setContentList(entry.getValue());
                menu.setDate(entry.getKey());
                menuList.add(menu);
            }
        }

        eatingHouse.setMenuList(menuList);
        eatingHouseService.update(eatingHouse);
    }
}
