package parser.feta;

import domain.common.BaseEatingHouseHandler;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FetaHandler extends BaseEatingHouseHandler {
    private static Pattern getDateFromString = Pattern.compile("(\\d{2}/\\d{2})");

    private static LocalDate parseWithDefaultYear(String stringWithoutYear) {
        LocalDate now = LocalDate.now(ZoneId.of("UTC"));
        int currentYear = now.getYear();

        DateTimeFormatter parseFormatter = new DateTimeFormatterBuilder()
                .appendPattern("dd/MM")
                .parseDefaulting(ChronoField.YEAR, currentYear)
                .toFormatter(Locale.ENGLISH);

        LocalDate dateTime = LocalDate.parse(stringWithoutYear, parseFormatter);
//        if (dateTime.isAfter(now)) {
//            dateTime = dateTime.minusYears(1);
//        }
        return dateTime;
    }

    public static LocalDate parseStringToDate(String date) {
        Matcher matcher = getDateFromString.matcher(date);
        if (matcher.find()) {
            LocalDate output = parseWithDefaultYear(matcher.group(0));
            System.out.println(" AAAAA " + LocalDate.from(output));
            return LocalDate.from(output);
        }
        return null;
    }
}
