package parser.feta;

import domain.common.BaseEatingHouseHandler;
import domain.content.Content;
import domain.content.ContentService;
import domain.eatingHouse.EatingHouse;
import domain.eatingHouse.EatingHouseService;
import domain.menu.Menu;
import domain.menu.MenuHelper;
import domain.menu.MenuService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FetaParser {
    public static final String BASE_URL = "http://www.feta.olsztyn.pl/";
    public static final String NAME = "Feta";
    public static final String CONTENT_URL = "dzien/menu.html";

    private static final String STATIC_CONTENT_CSS_QUERY = "div.content_column:nth-child(2) > div.news-mini";
    private static final String DAILY_CONTENT_CSS_QUERY = "div.menu_position > :not(div.last)";
    private static final String DAILY_CONTENT_DATE_CSS_QUERY = "body > b > font";

    private final ContentService contentService;
    private final EatingHouseService eatingHouseService;
    private final MenuService menuService;
    private final EatingHouse eatingHouse;
    private final Document document;

    public FetaParser() throws IOException {
        contentService = new ContentService();
        eatingHouseService = new EatingHouseService();
        menuService = new MenuService();
        eatingHouse = eatingHouseService.findByName(NAME);
        document = Jsoup.connect(BASE_URL + CONTENT_URL).get();
    }

    private Content parseStaticContent() {
        StringBuilder stringBuilder = new StringBuilder();
        Elements paragraphs = document.select(STATIC_CONTENT_CSS_QUERY);

        for (Element paragraph : paragraphs) {
            if (paragraph.hasText())
                stringBuilder.append(paragraph.text()).append("\n");
        }

        Content content = new Content();
        content.setType("STATIC");
        content.setContent(stringBuilder.toString());

        System.out.println(stringBuilder.toString());

        return content;
    }

    private HashMap<LocalDate, List<Content>> parseDailyContent() {
        HashMap<LocalDate, List<Content>> output = new HashMap<>();

        Elements paragraphsWithContent = document.select(DAILY_CONTENT_CSS_QUERY);

        List<Content> contentList;
        Content content;

        for (int i = 0; i < paragraphsWithContent.size(); i += 2) {
            if (paragraphsWithContent.get(i + 1).text().contains("W opracowaniu") ||
                    paragraphsWithContent.get(i + 1).text().contains("Nieczynne"))
                continue;
            content = new Content();
            contentList = new ArrayList<>();

            content.setContent(paragraphsWithContent.get(i + 1).text());
            content.setType("DAILY");
            contentList.add(content);

            output.put(FetaHandler.parseStringToDate(paragraphsWithContent.get(i).text()), contentList);
        }

        output.forEach(
                (key, value) -> {
                    System.out.println(key.toString() + " DATA");
                });

        return output;
    }

    public void parseEatingHouse() {
        HashMap<LocalDate, List<Content>> dailyContent = parseDailyContent();
        Content staticContent = parseStaticContent();

        List<Menu> loadedMenuList = eatingHouse.getMenuList();

        List<Menu> menuList = new ArrayList<>();

        if (dailyContent.get(BaseEatingHouseHandler.getCurrentDayLocalDate()) == null ||
                dailyContent.get(BaseEatingHouseHandler.getCurrentDayLocalDate()).size() < 1) {
            Menu menu = new Menu();
            List<Content> contentList = new ArrayList<>();
            menu.setDate(BaseEatingHouseHandler.getCurrentDayLocalDate());
            menu.setEatingHouse(eatingHouse);

            contentList.add(staticContent);
            menu.setContentList(contentList);
            menuService.persist(menu);
        } else
            for (Map.Entry<LocalDate, List<Content>> entry : dailyContent.entrySet()) {
                Menu menu;
                if (entry.getKey().equals(BaseEatingHouseHandler.getCurrentDayLocalDate()))
                    entry.getValue().add(staticContent);

                if (MenuHelper.containsDate(loadedMenuList, entry.getKey())) {
                    menu = MenuHelper.getMenuByDate(loadedMenuList, entry.getKey());
                    List<Content> contentList = menu.getContentList();

                    int i = 0;
                    for (; i < entry.getValue().size(); i++) {
                        if (i < contentList.size()) {
                            contentList.get(i).setContent(entry.getValue().get(i).getContent());
                            contentList.get(i).setType(entry.getValue().get(i).getType());
                            contentService.update(contentList.get(i));
                        } else {
                            Content newContent = new Content();
                            newContent.setContent(entry.getValue().get(i).getContent());
                            newContent.setType(entry.getValue().get(i).getType());
                            newContent.setMenu(menu);
                            contentService.persist(newContent);
                        }
                    }

                    for (int j = i; j < contentList.size(); j++) {
                        contentService.delete(contentList.get(j).getId());
                    }
                } else {
                    menu = new Menu();
                    menu.setContentList(entry.getValue());
                    menu.setDate(entry.getKey());
                    menuList.add(menu);
                }
            }

        eatingHouse.setMenuList(menuList);
        eatingHouseService.update(eatingHouse);
//        parseDailyContent();
//        parseStaticContent();
    }
}
