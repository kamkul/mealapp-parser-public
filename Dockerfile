FROM openjdk:8-jdk-alpine
COPY target/dependency-jars /run/dependency-jars
ADD target/application.jar /run/application.jar

ENTRYPOINT java -jar run/application.jar -D exec.mainClass="parser.Application"